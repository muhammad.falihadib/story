from django import forms
from .models import MyCourse, MyActivity

class InputForm(forms.ModelForm):
    class Meta:
        model = MyCourse
        fields = ['mata_kuliah', 'dosen_pengajar', 'jumlah_sks', 'deskripsi', 'semester_tahun', 'ruang_kelas']

    error_messages = {
        'required' : 'Wajib dimasukkan'
    }

    course_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama mata kuliah'
	}

    dosen_attrs = {
		'type' : 'text',
		'placeholder' : 'Dosen pengajar'
	}
    
    sks_attrs = {
		'type' : 'text',
		'placeholder' : 'Jumlah SKS'
	}
    
    desc_attrs = {
		'type' : 'text',
		'placeholder' : 'Deskripsi mata kuliah'
	}

    smst_attrs = {
		'type' : 'text',
		'placeholder' : 'Semester tahun'
	}
    
    kelas_attrs = {
		'type' : 'text',
		'placeholder' : 'Ruang kelas'
	}
        
    mata_kuliah = forms.CharField(label='', required= True, max_length=50, widget=forms.TextInput(attrs=course_attrs))
    dosen_pengajar = forms.CharField(label='', required= True, max_length=30, widget=forms.TextInput(attrs=dosen_attrs))
    jumlah_sks = forms.CharField(label='', required= True, max_length=1, widget=forms.TextInput(attrs=sks_attrs))
    deskripsi = forms.CharField(label='', required= True, max_length=800, widget=forms.Textarea(attrs=desc_attrs))
    semester_tahun = forms.CharField(label='', required= True, max_length=15, widget=forms.TextInput(attrs=smst_attrs))
    ruang_kelas = forms.CharField(label='', required= True, max_length=5, widget=forms.TextInput(attrs=kelas_attrs))

class ActivityForm(forms.ModelForm):
  
    class Meta:
        model = MyActivity
        fields = ['nama_kegiatan']

    error_messages = {
        'required' : 'Wajib dimasukkan'
    }

    activity_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama kegiatan'
	  }
    
    nama_kegiatan = forms.CharField(label='', required= True, max_length=30, widget=forms.TextInput(attrs=activity_attrs))