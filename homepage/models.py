from django.db import models

# Create your models here.

class MyCourse(models.Model):
    mata_kuliah = models.CharField(max_length=50)
    dosen_pengajar = models.CharField(max_length=30) 
    jumlah_sks = models.CharField(max_length=1)
    deskripsi = models.CharField(max_length=800)
    semester_tahun = models.CharField(max_length=15)
    ruang_kelas = models.CharField(max_length=5)

class MyActivity(models.Model):
    nama_kegiatan = models.CharField(max_length=30)

class Participants(models.Model):
    nama_peserta = models.CharField(max_length=30)  