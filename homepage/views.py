from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import InputForm, ActivityForm
from .models import MyCourse, MyActivity

response = {}
# Create your views here.
def home(request):
    return render(request, 'home.html')
def aboutme(request):
    return render(request, 'aboutme.html')
def aboutme2(request):
    return render(request, 'aboutme2.html')
def books(request):
    return render(request, 'books.html')
def movies(request):
    return render(request, 'movies.html')
def ayah(request):
    return render(request, 'ayah.html')
def knivesout(request):
    return render(request, 'knivesout.html')
def mystudy(request):
    courses = MyCourse.objects.all()
    response['courses'] = courses
    return render(request, 'mystudy.html', response)
def addcourse(request):
    response = {'inputForm' : InputForm}
    return render(request, 'addcourse.html', response)
def savecourse(request):
    form = InputForm(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/mystudy')
def detail(request, pk):
    detail = {'matkul' : MyCourse.objects.get(pk=pk)}
    return render(request, 'detail.html', detail)
def delete(request, pk):
    query = MyCourse.objects.get(pk=pk)
    query.delete()
    return HttpResponseRedirect('/mystudy')
def myactivity(request):
    activities = MyActivity.objects.all()
    activities_dict = {
        'activities': activities
    }
    response['activities'] = activities_dict
    return render(request, 'myactivity.html', activities_dict)
def addactivity(request):
    response = {'activityForm' : ActivityForm}
    return render(request, 'addactivity.html', response)
def saveactivity(request):
    form = ActivityForm(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/myactivity')