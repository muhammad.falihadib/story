from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('aboutme2/', views.aboutme2, name='aboutme2'),
    path('books/', views.books, name='books'),
    path('books/ayah/', views.ayah, name='ayah'),
    path('movies/', views.movies, name='movies'),
    path('movies/knives-out/', views.knivesout, name='knivesout'),
    path('mystudy/', views.mystudy, name='mystudy'),
    path('mystudy/addcourse/', views.addcourse, name='addcourse'),
    path('savecourse/', views.savecourse),
    path('mystudy/<int:pk>/', views.detail),
    path('delete/<int:pk>/', views.delete),
    path('myactivity/', views.myactivity, name='myactivity'),
    path('myactivity/addactivity/', views.addactivity, name='addactivity'),
    path('saveactivity/', views.savecourse),
]